//
//  VorfreudeTests.swift
//  VorfreudeTests
//
//  Created by Ahmet Yalcinkaya on 11.12.2017.
//  Copyright © 2017 Vorfreude. All rights reserved.
//

import XCTest
@testable import Vorfreude

class VorfreudeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDays() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let vc = MainViewController()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let thisXmas = formatter.date(from: "2017-12-24")!
        let nextXmas = formatter.date(from: "2018-12-24")!
        var date = formatter.date(from: "2017-12-22")!
        var days = vc.calculateDays(from: date, to: thisXmas)
        assert(days == 2)
        
        date = formatter.date(from: "2017-12-19")!
        days = vc.calculateDays(from: date, to: thisXmas)
        assert(days == 5)
        
        date = formatter.date(from: "2017-11-19")!
        days = vc.calculateDays(from: date, to: thisXmas)
        assert(days == 35)
        
        date = formatter.date(from: "2017-12-24")!
        days = vc.calculateDays(from: date, to: nextXmas)
        assert(days == 365)
        
        date = formatter.date(from: "2017-12-30")!
        days = vc.calculateDays(from: date, to: nextXmas)
        assert(days == 359)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
